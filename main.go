package main


import (
	"SenML_CV2/services"
	"flag"
	"fmt"
	"github.com/golang/glog"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
)

func init() {
	//glog
	//create logs folder
	absPath, _ := filepath.Abs(filepath.Join("lp-cms-client","logs"))
	err := os.Mkdir(absPath, 0777)
	if err !=nil{
		fmt.Println("Folder Logs CMS already exist, Continue writing log to this folder")
	}
	flag.Lookup("stderrthreshold").Value.Set("[INFO|WARN|FATAL]")
	flag.Lookup("logtostderr").Value.Set("false")
	flag.Lookup("alsologtostderr").Value.Set("true")
	flag.Lookup("log_dir").Value.Set(absPath)
	glog.MaxSize = 1024 * 1024 * 258
	flag.Lookup("v").Value.Set(fmt.Sprintf("%d", 8))
	flag.Parse()

}
func main() {
	flag.Parse()
	defer func() {
		if err := recover(); err != nil {
			glog.Error("-------------RECOVER err: ", err)
		}
	}()
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	err:=services.ConnectMqtt()
	if err != nil {
		glog.Info("Failed, exit")
		os.Exit(1)
	} else {
		glog.Info("OK")
	}
	glog.Flush()
	<-c
}
